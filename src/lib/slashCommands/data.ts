export const excludeData = {
	name: "exclude",
	description: "Excludes you from being targeted by dad-bot. Execute command again to reverse this action.",
};