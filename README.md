#  Welcome to Kaiki Deishu Bot

### KaikiBot

[Add Kaiki to your server](https://discord.com/oauth2/authorize?client_id=714695773534814238&scope=bot)

# Guide

[Guide to setup bot](./GUIDE.md).

# Contributing

Consider contributing. I appreciate all the help I can get!
  1. Fork the bot!
  1. Create your feature branch: `git checkout -b cool-new-branch`
  1. Commit your changes: `git commit -am "Added x feature!"`
  1. Push to the branch: `git push origin My-new-feature`
  1. Submit a pull request!

# I owe some amazing people thanks, and more!
- Huge thanks to @Arvfitii for helping me whenever im in need!
- Thanks to @rjt-rockx on Discord for so much help and time!
- Should also mention @shivaco for help <3
- Thanks also to @Kwoth <3

# About
### A bit about this project
This bot was my first entry into javascript, nodejs, discordjs, typeScript and programming/coding overall. It has been very fun and also frustrating. If you do find unoptimized code and or flaws or other inherently bad code - I would be very happy to merge changes and try to learn from my own mistakes. Commenting the code helps! I will try to do so as well.
